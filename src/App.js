import React from 'react';
import Header from './Components/Header/header';
import Navbar from './Components/Navbar/navbar';
import ArticleList from './Components/ArticleList/articleList';
import articles from "../src/fixtures";
import 'bootstrap/dist/css/bootstrap.css';
import { Route } from 'react-router-dom';
import {User} from './Components/User/user';
import {Page} from './Components/Page/page';
import { setYear } from './Action/PageAction/PageActions';
import { connect } from 'react-redux';

class App extends React.Component {
  state = {
    navbarClicked: true
  }

  render() {
    const { user, page, setYearAction } = this.props;
    console.log("props", this.props, "props")
    return (
      <div className="App container">
        <Navbar color="yellow" />
        <div className="jumbotron d-flex text-center">
          <div className="container">
            <div className="row">
              <User name={user.name} />
              <Page photos={page.photos} year={page.year} setYear={setYearAction} />
            </div>
          </div>
        </div>
        <div>
          <Route path='/' exect />
          <Route path='/header' component={Header} />
          <Route path="/articles" component={ArticleList} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => {
  return {
    user: store.user,
    page: store.page,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setYearAction: year => dispatch(setYear(year))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)