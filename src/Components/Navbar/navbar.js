import React from 'react'
import PropTypes from 'prop-types';

class Navbar extends React.Component{
    
    constructor(props) {
        super(props);

        this.state={clicked:false, color:'yellow'};

    }

    render(){
        const {color, onClick} =this.props;
        return(
            
            <div>
                {/* <h1 style={{color:{color}}}>Navbar </h1>
                <button onClick={this.handlerClick.bind(this)}>gmyak</button> */}
                <nav className="navbar navbar-expand-lg navbar-light bg-light my-0">
                    <div className="collapse navbar-collapse">
                        <ul className="navbar-nav">
                            <li className="nav-item text-uppercase">
                                <a className="nav-link h3" href="/">exect</a>
                            </li>
                            <li className="nav-item text-uppercase">
                                <a className="nav-link h3" href="/articles">articles</a>
                            </li>
                            <li className="nav-item text-uppercase">
                                <a className="nav-link h3" href="/header">header</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        );
    }

    handlerClick=()=>{
        this.setState(this.state, ()=>{
            this.state.clicked = !this.state.clicked;
            this.state.olor="yellow";
        });

        this.props.onClick(this.state.clicked);
    }
}

export default Navbar;