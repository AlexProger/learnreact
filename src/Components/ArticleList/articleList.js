import React from 'react';
import Article from '../Article/article';
import "./articleList.css"
import articles from "../../fixtures";

export default   function ArticleList(){
        const articleElements = articles.map((article, index)=>
            <li className="article-list__li" key={article.id}>
                <Article article={article} defaultOpen={index===0}/>
            </li>)
    return(
        <ul className="px-0">
            {articleElements}
        </ul>
    );
   }
